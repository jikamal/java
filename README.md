## Java Dockerfile

The repository is a mirror of the [dockerfile/java](https://github.com/dockerfile/java) repository, which was deprecated in early 2015 and replaced with the docker-sanctioned [java](https://registry.hub.docker.com/_/java/). Sadly, the new Java repo lacks support for Oracle Java, per the explanation in the "Why is this only OpenJDK/OpenJRE?" section of the repo's README.

The only base images in the docker world that I trust are [OFFICIAL REPOS](https://docs.docker.com/docker-hub/official_repos/). Therefore, I created this repo to provide solid base for Java apps that I support that have Oracle Java as a requirement. 

### Base Docker Image

* [ubuntu](https://registry.hub.docker.com/_/ubuntu/)


### Docker Tags

`java` provides multiple tagged images:

* `latest` (default): Oracle Java 8 JDK
* `oracle-java8`: Oracle Java 7 JDK
* `oracle-java7`: Oracle Java 8 JDK

For example, you can run a `Oracle Java 7` container with the following command:

    docker run -it --rm thomasbjackson/java:oracle-java7 java -version


### Installation and Usage

See README in the [Java OFFICIAL REPO](https://registry.hub.docker.com/_/java/)
